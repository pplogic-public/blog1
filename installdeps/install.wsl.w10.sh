#!/bin/bash
# works on wsl2 
SCRIPTPICK="Script that installs hugo and dependencies in wsl2"
HVERSION="0.71.1"
HFILE="hugo_${HVERSION}_Linux-64bit.deb"
TMPDIR=~/HUGOTMP

# Function to print in specified color
colorprintf () {
    case $1 in
        "red") tput setaf 1;;
        "green") tput setaf 2;;
        "orange") tput setaf 3;;
        "blue") tput setaf 4;;
        "purple") tput setaf 5;;
        "cyan") tput setaf 6;;
        "gray" | "grey") tput setaf 8;;
        "white") tput setaf 7;;
    esac
    echo "$2";
    tput sgr0
}

colorprintf purple "Running $SCRIPTPICK"

# install hugo dependencies
colorprintf orange "Installing Apt Updates and dependencies"

sudo apt update > /dev/null
sudo apt upgrade -y > /dev/null
sudo apt install -y wget git curl dpkg jq vim tmux > /dev/null
sudo apt autoremove -y > /dev/null

# fetch hugo and install 
colorprintf orange "Fetching and installing Hugo $HVERSION"
mkdir -p $TMPDIR
cd $TMPDIR
wget -q --no-verbose https://github.com/gohugoio/hugo/releases/download/v${HVERSION}/$HFILE
sudo dpkg -i $HFILE
cd ..
# cleanup
rm -rf $TMPDIR

colorprintf green "Done"