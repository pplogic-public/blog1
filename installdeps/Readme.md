# How to work with this site locally 
## Install Dependencies
### Linux /Mac
For apt based distributions just run 
```shell
source ./installdeps/install.wsl.w10.sh
```
For any other distro or macos follow these [instructions](https://gohugo.io/getting-started/installing/)
### Windows 
#### Install wsl 
Follow the instructions from this [link](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to install Ubuntu 18.04 or 20.04 
#### Install VScode 
VSCode makes working with files in WSL so easy 
Fetch the [Latest Version](https://code.visualstudio.com/download#)
Launch it and install [Remote WSL addon](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)

#### Install Hugo in WSL
clone this repo in wsl home folder (not in a windows folder)
Navigate to wsl home folder by typing 
```shell
cd ~/
```
Clone this repo by typing
```shell
git clone https://gitlab.com/pplogic-public/blog1.git
```
and run
```shell
cd blog1
source ./installdeps/install.wsl.w10.sh
```
## How to launch this site 
From the root of the repo run 
```shell
hugo server -D
```
Now open your web browser [to this link](http://localhost:1313/) to view the site 

## How to Work on this site 

WIP
